package com.tsf.api.acct.request;

import com.tsf.common.enums.TestTypeEnum;
import com.tsf.common.valid.EumValid;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Created by bpf on 2020/12/22.
 */
@Data
public class ReqBetBillCreateBusParam implements Serializable {


    /**
     * 测试类型 EumTestType   1成功，2超时，3生产者异常，4生产者掉线
     */
    @NotEmpty(message = "type不能为空")
    @EumValid(target = TestTypeEnum.class, message = "请填写正确的type值")
    private String type;

    /**
     * 订单编号
     */
    private String orderNo;


    /**
     * 订单金额
     */
    private Long orderMoney;

    /**
     * 期结时间
     */
    private Long orderFinalTime;

    @NotBlank(message = "数据不能为空")
    private String desc;

}
