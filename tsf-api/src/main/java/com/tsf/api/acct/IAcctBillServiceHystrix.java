package com.tsf.api.acct;

import com.tsf.api.acct.request.ReqBetBillCreateBusParam;
import com.tsf.common.exception.CommonErrorMessage;
import com.tsf.common.result.R;
import com.tsf.common.result.REnum;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Created by bpf on 2020/12/22.
 */
@Component
@Slf4j
public class IAcctBillServiceHystrix implements FallbackFactory<IAcctBillService> {
    @Override
    public IAcctBillService create(Throwable throwable) {
       return new IAcctBillService() {
           @Override
           public R betBillCreate(ReqBetBillCreateBusParam feignRequest) throws RuntimeException {
//               log.error(REnum.SERVICE_HYSTRIX.getMessage(),throwable);
               return R.error(new CommonErrorMessage(REnum.SERVICE_HYSTRIX,String.format("%s",throwable)));
           }

           @Override
           public R betBillCreate2() throws RuntimeException {
               return R.error(new CommonErrorMessage(REnum.SERVICE_HYSTRIX,String.format("%s",throwable)));
           }
       };
    }
}
