package com.tsf.api.acct;

import com.tsf.api.acct.constant.AcctConstant;
import com.tsf.api.acct.request.ReqBetBillCreateBusParam;
import com.tsf.common.result.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Created by bpf on 2020/12/22.
 */

@FeignClient(value = AcctConstant.APPLICATION_ACCT_BILL,fallbackFactory =IAcctBillServiceHystrix.class)
public interface IAcctBillService {

    String API_PREFIX = "/acct";

    @RequestMapping(method = RequestMethod.POST,value = API_PREFIX + "/betBillCreate",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    R betBillCreate(@Validated @RequestBody ReqBetBillCreateBusParam feignRequest)  throws RuntimeException ;


    @RequestMapping(method = RequestMethod.GET,value = API_PREFIX + "/betBillCreate2")
    R betBillCreate2()  throws RuntimeException ;



}
