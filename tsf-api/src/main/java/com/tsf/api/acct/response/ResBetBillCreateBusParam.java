package com.tsf.api.acct.response;


import lombok.Data;



/**
 * Created by bpf on 2020/12/22.
 */
@Data
public class ResBetBillCreateBusParam {
    private String transNo;
}
