/*
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements.  See the NOTICE file distributed with
 *   this work for additional information regarding copyright ownership.
 *   The ASF licenses this file to You under the Apache License, Version 2.0
 *   (the "License"); you may not use this file except in compliance with
 *   the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

package com.tsf.common.exception;

import com.tsf.common.result.REnum;

/**
 * Created by west on 2020/12/22.
 */
public class CommonException extends RuntimeException {

    private static final long serialVersionUID = 8068509879445395353L;

    private REnum rEnum;
    private String message;

    /**
     * Instantiates a new Feign exception.
     *
     * @param e the e
     */
    public CommonException(final Throwable e) {
        super(e);
    }

    /**
     * Instantiates a new Feign exception.
     *
     * @param message the message
     */
    public CommonException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new Feign exception.
     *
     * @param message   the message
     * @param throwable the throwable
     */
    public CommonException(final String message, final Throwable throwable) {
        super(message, throwable);
    }


    public CommonException(REnum r, String message) {
        super(message);
        this.rEnum = r;
        this.message = r.getMessage();
    }

    public REnum getrEnum() {
        return rEnum;
    }

    public void setrEnum(REnum rEnum) {
        this.rEnum = rEnum;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
