package com.tsf.common.exception;

import com.tsf.common.result.REnum;
import lombok.Data;

@Data
public class CommonErrorMessage {
    private Integer errorCode;
    private String errorMessage;
    private String errorDetails;

    public CommonErrorMessage(Integer errorCode,String errorMessage, String errorDetails) {
        this.errorCode=errorCode;
        this.errorMessage = errorMessage;
        this.errorDetails = errorDetails;
    }

    public CommonErrorMessage(REnum r,String errorDetails){
        this.errorCode=r.getCode();
        this.errorMessage = r.getMessage();
        this.errorDetails = errorDetails;
    }

}

