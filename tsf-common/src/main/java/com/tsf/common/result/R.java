/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tsf.common.result;

import lombok.Data;

import java.io.Serializable;


/**
 * Created by west on 2020/12/22.
 */
@Data
public class R implements Serializable {

    private static final long serialVersionUID = -2792556188993845048L;
    

    private Integer code;

    private String message;

    private Object data;






    /**
     * Instantiates a new Feign result.
     */
    public R() {

    }

    public R(final Integer code, final String message, final Object data) {

        this.code = code;
        this.message = message;
        this.data = data;
    }

    private static R get(final int code, final String message, final Object data) {
        return new R(code, message, data);
    }










    /**
     * return success.
     * @return {@linkplain R}
     */
    public static R success() {
        return get(REnum.SUCCESS.getCode(),REnum.SUCCESS.getMessage(),null);
    }

    /**
     * return success.
     * @param data this is result data.
     * @return {@linkplain R}
     */
    public static R success(final Object data) {
        return get(REnum.SUCCESS.getCode(),REnum.SUCCESS.getMessage(),data);
    }

    /**
     * return success.
     *
     * @param message  this ext message.
     * @param data this is result data.
     * @return {@linkplain R}
     */
    public static R success(final String message, final Object data) {
        return get(REnum.SUCCESS.getCode(), message, data);
    }
    /**
     * Success Feign web result.
     *
     * @param code the code
     * @param message  the message
     * @param data the data
     * @return the Feign web result
     */
    public static R success(final int code, final String message, final Object data) {
        return get(code, message, data);
    }







    /**
     * return error.
     * @return {@linkplain R}
     */
    public static R error() {
        return get(REnum.ERROR.getCode(),REnum.ERROR.getMessage(),null);
    }

    /**
     * return error.
     * @param data this is result data.
     * @return {@linkplain R}
     */
    public static R error(final Object data) {
        return get(REnum.ERROR.getCode(),REnum.ERROR.getMessage(),data);
    }

    /**
     * return error.
     *
     * @param message  this ext message.
     * @param data this is result data.
     * @return {@linkplain R}
     */
    public static R error(final String message, final Object data) {
        return get(REnum.ERROR.getCode(), message, data);
    }
    /**
     * error Feign web result.
     *
     * @param code the code
     * @param message  the message
     * @param data the data
     * @return the Feign web result
     */
    public static R error(final int code, final String message, final Object data) {
        return get(code, message, data);
    }














}
