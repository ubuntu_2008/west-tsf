package com.tsf.common.result;


import lombok.Getter;
import lombok.RequiredArgsConstructor;


/**
 * Created by bpf on 2020/12/22.
 */

@Getter
@RequiredArgsConstructor
public enum REnum {


    /**
     * 主要用于内部服务响应
     */
    ERROR(0, "失败!"),

    SUCCESS(1, "成功!"),


    /**
     * 网关-签名验证失败
     */
    SIGN_IS_NOT_PASS(401, "签名未通过!"),

    /**
     * 网关-拒绝访问,如  请求文件过大，黑名单等
     */
    NO_ACCESS(403, "拒绝访问!"),

    /**
     * 网关-地址未匹配
     */
    URL_NOT_FIND(404, "请检查请求地址!"),

    /**
     * 网关-限流
     */
    TOO_MANY_REQUESTS(429, "您已经被限流，请稍后重试!"),

    /**
     * 网关-请求必须要有参数
     */
    HAVE_BODY_PARAM(431, "接口必须要有参数，请在body里面输入json格式！"),





    /**
     * 熔断降级
     */
    SERVICE_HYSTRIX(100, "服务降级"),


    SERVICE_ERROR(101, "系统内部错误"),

    /**
     * full selector type enum.
     */
    PARAM_ERROR(102, "您的参数错误,请检查相关文档!"),


    /**
     * Service result error feign result enum.
     */
    SERVICE_RESULT_ERROR(103, "服务调用异常，或者未返回结果"),

    /**
     * Service timeout feign result enum.
     */
    SERVICE_TIMEOUT(104, "服务调用超时"),

    /**
     * Sing time is timeout feign result enum.
     */
    SING_TIME_IS_TIMEOUT(105, "签名时间戳已经超过%s分钟!"),

    /**
     * Cannot find url feign result enum.
     */
    CANNOT_FIND_URL(106, "未能找到合适的调用url,请检查你的配置!"),

    /**
     * Cannot find selector feign result enum.
     */
    CANNOT_FIND_SELECTOR(107, "未能匹配选择器,请检查你的选择器配置！"),

    /**
     * The Cannot config springcloud serviceid.
     */
    CANNOT_CONFIG_SPRINGCLOUD_SERVICEID(108, "您并未配置或未匹配springcloud serviceId"),

    /**
     * The Springcloud serviceid is error.
     */
    SPRINGCLOUD_SERVICEID_IS_ERROR(109, "springCloud serviceId 不存在或者配置错误！或者注册中心配置错误! ");


    private final int code;

    private final String message;
}
