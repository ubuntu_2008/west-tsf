分组
https://didispace-wx.blog.csdn.net/article/details/103516154?utm_medium=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.control&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.control

https://www.jianshu.com/p/253642121891

@Validated与@Valid的简单对比说明
https://my.oschina.net/didispace/blog/4795459
@Valid注解与@Validated注解功能大部分类似；
两者的不同主要在于:@Valid属于javax下的，而@Validated属于spring下；
@Valid支持嵌套校验、而@Validated不支持，@Validated支持分组，而@Valid不支持。
笔者这里只简单介绍@Validated的使用时机。

hibernate的校验模式
1、普通模式（默认是这个模式）
普通模式(会校验完所有的属性，然后返回所有的验证失败信息)

2、快速失败返回模式
快速失败返回模式(只要有一个验证失败，则返回)


@NotNull 任何对象的value不能为null

@NotEmpty 集合对象的元素不为0，即集合不为空，也可以用于字符串不为null

@NotBlank 只能用于字符串不为null，并且字符串trim()以后length要大于0

/**
 * 集合校验
 */
@NotEmpty(message = "成绩必填")
private List<Double> scores;

/**
 * 显示状态[0-不显示；1-显示]
 */
@NotNull(message = "显示状态不能为空")
private Integer showStatus;

/**
 * 状态码
 */
@Pattern(regexp = "0[0123]", message = "状态只能为00或01或02或03")
private String status;

@NotEmpty(message="支付完成时间不能空")
@Size(max=14,message="支付完成时间长度不能超过{max}位")
private String payTime;

/**
 * 邮箱
 */
@NotBlank(message = "邮箱不能为空")
@NotNull(message = "邮箱不能为空")
@Email(message = "邮箱格式错误")
private String email;


@Email(regexp = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z0-9]{2,6}$", message =  "邮件格式错误")
private String email;

/**
 * 手机号
 */
@NotNull(message = "手机号不能为空")
@NotBlank(message = "手机号不能为空")
@Pattern(regexp ="^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误")
@Max(value = 11,message = "手机号只能为{max}位")
@Min(value = 11,message = "手机号只能为{min}位")
private String mobileNo;


/**
 * 年龄
 */
@Min(1)
@Max(120)
private Integer age;

@Min(value = 18, message = "年龄最小为18")
@Max(value = 60, message = "年龄最大为60")
int age;

/**
 * 密码
 */
@NotBlank(message = "密码不能为空")
@Size(min = 6, max = 16)
private String password;


/**
 * 用户名
 */
@NotNull(message = "用户名不能为空")
@Length(min = 6, max = 18, message = "用户名长度必须为6-18位")
private String username;

/**
 * 条款
 */
@AssertTrue(message = "必须同意条款")
boolean agree;


/**
 * 性别
 */
@Pattern(regexp = "((^Man$|^Woman$|^UGM$))", message = "sex 值不在可选范围")
@NotNull(message = "sex 不能为空")
private String sex;
正则表达式说明：
- ^string : 匹配以 string 开头的字符串
- string$ ：匹配以 string 结尾的字符串
- ^string$ ：精确匹配 string 字符串
- ((^Man$|^Woman$|^UGM$)) : 值只能在 Man,Woman,UGM 这三个值中选择

/**
 * Dubbo
 */
@DecimalMax(value="2.5")
@DecimalMin(value="1.0")
private Double height;


//验证身份证格式
@Pattern(regexp = "^(\\d{18,18}|\\d{15,15}|(\\d{17,17}[x|X]))$", message = "身份证格式错误")
private String idCard;

