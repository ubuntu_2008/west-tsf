package com.tsf.common.valid;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

public class ListNotEmptyValidatorHandler implements ConstraintValidator<ListGtOne, Collection> {

    public void initialize(ListGtOne listNotEmpty) {

    }

    public boolean isValid(Collection collection, ConstraintValidatorContext constraintValidatorContext) {
        if(collection == null || collection.size()<2){
            return false;
        }
        return true;
    }
}
