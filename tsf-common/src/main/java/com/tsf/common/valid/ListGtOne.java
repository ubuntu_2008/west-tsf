package com.tsf.common.valid;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
//**ListNotEmptyValidator为对应ListGtOne的实现**
@Constraint(
        validatedBy = {ListNotEmptyValidatorHandler.class}
)
public @interface ListGtOne {
    /**
     *以下方法仿照@NotBlank注释
     */
    String message() default "列表数据为空！";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}