package com.tsf.common.valid;

import com.tsf.common.exception.CommonException;
import com.tsf.common.result.REnum;
import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.HibernateValidatorConfiguration;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

public class ValidatorUtil {


//    快速失败(Fail Fast)配置
    private static Validator validator = ((HibernateValidatorConfiguration) Validation
            .byProvider(HibernateValidator.class).configure()).failFast(true).buildValidatorFactory().getValidator();

    /**
     * 实体校验
     *
     * @param obj
     * @throws CommonException
     */
    public static <T> void validate(T obj,Class<?>... clazz) throws CommonException {
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(obj, clazz==null?new Class[0]:clazz);
        if (constraintViolations.size() > 0) {
            ConstraintViolation<T> validateInfo = (ConstraintViolation<T>) constraintViolations.iterator().next();
            // validateInfo.getMessage() 校验不通过时的信息，即message对应的值
            throw new CommonException(REnum.PARAM_ERROR, validateInfo.getMessage());
        }
    }
}
