package com.tsf.common.enums;

public enum TestTypeEnum {

    TEST_1("1", "成功"),
    TEST_2("2", "超时"),
    TEST_3("3", "生产者异常");


    private String code;
    private String message;

    TestTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static TestTypeEnum getByValue(String value) {
        for (TestTypeEnum code : values()) {
            if (code.getCode().equals(value)) {
                return code;
            }
        }
        return null;
    }
}