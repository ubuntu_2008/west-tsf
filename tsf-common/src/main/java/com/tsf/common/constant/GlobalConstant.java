package com.tsf.common.constant;

public class GlobalConstant {
    public static final String DEFAULT_CHARSET = "UTF-8";
    public static final long DATA_CENTER_ID = 1;
    public static final long MACHINE_ID = 1;
}
