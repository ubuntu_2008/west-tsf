package com.tsf.acct.tool.controller;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "lottery")
public class TestConfigEntity {
    private String playa;
    private String playb;
    private String playc;

    public String getPlaya() {
        return playa;
    }

    public void setPlaya(String playa) {
        this.playa = playa;
    }

    public String getPlayb() {
        return playb;
    }

    public void setPlayb(String playb) {
        this.playb = playb;
    }

    public String getPlayc() {
        return playc;
    }

    public void setPlayc(String playc) {
        this.playc = playc;
    }
}
