package com.tsf.acct.tool.controller;


import com.tsf.api.acct.IAcctBillService;
import com.tsf.api.acct.request.ReqBetBillCreateBusParam;
import com.tsf.common.config.ConsulService;
import com.tsf.common.utils.JsonUtils;
import com.tsf.common.utils.SnowFlakeWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RefreshScope
public class AcctToolController {



    @Autowired
    private TestConfigEntity testConfigEntity;


    @Autowired
    private IAcctBillService iAcctBillService;

    /**
     * @param type EumTestType
     *             1调用成功
     *             2调用超时
     *             3调用生产者异常、生产者掉线
     * @return
     */


    @ResponseBody
    @GetMapping("/test/betBillCreate")
    public String betBillCreate(String type) {
        ReqBetBillCreateBusParam reqBetBillCreateBusParam = new ReqBetBillCreateBusParam();
        reqBetBillCreateBusParam.setOrderFinalTime(System.currentTimeMillis());
        reqBetBillCreateBusParam.setOrderMoney(100L);
        reqBetBillCreateBusParam.setOrderNo(SnowFlakeWorker.getSnowFlakeWorker().nextId() + "");
        reqBetBillCreateBusParam.setType(type);
        reqBetBillCreateBusParam.setDesc("c");
        return JsonUtils.toJson(iAcctBillService.betBillCreate(reqBetBillCreateBusParam));
    }


    @ResponseBody
    @GetMapping("/test/config")
    public String betBillCreate1() {
        return JsonUtils.toJson(testConfigEntity);
    }
}
