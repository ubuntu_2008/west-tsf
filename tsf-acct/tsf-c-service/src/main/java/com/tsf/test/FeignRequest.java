package com.tsf.test;

import com.tsf.api.acct.request.ReqBetBillCreateBusParam;
import lombok.Data;

import javax.validation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
public class FeignRequest<T> {


    @Valid
    private T feignBusParam;


    private static FeignRequest getBean() {
        FeignRequest bean = new FeignRequest();
        ReqBetBillCreateBusParam reqTestValidParam=new ReqBetBillCreateBusParam();
        reqTestValidParam.setType("3");
        bean.setFeignBusParam(reqTestValidParam);
        return bean;
    }

    public static void main(String[] args) {
        FeignRequest feignRequest = getBean();
        List<String> validate = validate(feignRequest);
        if(validate.size()>0) {
            validate.forEach(row -> {
                System.out.println("验证失败 -- " + row.toString());
            });
            return;
        }

        System.out.println("验证通过");

    }


    private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

    public static <T> List<String> validate(T t) {
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(t);

        List<String> messageList = new ArrayList<>();
        for (ConstraintViolation<T> constraintViolation : constraintViolations) {
            messageList.add(constraintViolation.getPropertyPath()+"="+constraintViolation.getMessage());
        }
        return messageList;
    }
}
