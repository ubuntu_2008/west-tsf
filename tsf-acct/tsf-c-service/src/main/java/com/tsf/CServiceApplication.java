package com.tsf;

import com.tsf.acct.tool.controller.TestConfigEntity;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.tsf.annotation.EnableTsf;

/**
 * Created by bpf on 2019/9/3.
 * 测试 测试 测试
 */
@SpringBootApplication
@EnableFeignClients
@EnableTsf
@EnableConfigurationProperties({TestConfigEntity.class})
public class CServiceApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(CServiceApplication.class)
                .run(args);
    }


}
