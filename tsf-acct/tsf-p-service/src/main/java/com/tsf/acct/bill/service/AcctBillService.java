package com.tsf.acct.bill.service;

import com.tsf.api.acct.IAcctBillService;
import com.tsf.api.acct.request.ReqBetBillCreateBusParam;
import com.tsf.api.acct.response.ResBetBillCreateBusParam;
import com.tsf.common.enums.TestTypeEnum;
import com.tsf.common.result.R;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AcctBillService implements IAcctBillService {


    /**
     * 测试
     * @param feignRequest
     * @return
     * @throws RuntimeException
     */
    @Override
    public R betBillCreate(ReqBetBillCreateBusParam feignRequest) throws RuntimeException {
        TestTypeEnum testTypeEnum = TestTypeEnum.getByValue(feignRequest.getType());
        switch (testTypeEnum){
            /**
             * 成功
             */
            case TEST_1:

                break;
            /**
             * 超时
             */
            case TEST_2:
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            /**
             * 生产者异常
             */
            case TEST_3:
                Integer.parseInt("模拟生产者异常");
                break;

            default:
                break;


        }
        ResBetBillCreateBusParam resBetBillCreateBusParam = new ResBetBillCreateBusParam();
        resBetBillCreateBusParam.setTransNo("BL"+feignRequest.getOrderNo());
        return  R.success(resBetBillCreateBusParam);
    }

    @Override
    public R betBillCreate2() throws RuntimeException {
        return R.success("2");
    }


}
