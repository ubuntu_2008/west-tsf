//package com.tsf.acct.bill.controller;
//
//import com.tsf.acct.bill.service.AcctBillService;
//import com.tsf.feign.FeignRequest;
//import com.tsf.feign.FeignResponse;
//import com.tsf.api.acct.IAcctBillService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.validation.Valid;
//
///**
// * Created by bpf on 2019/4/12.
// */
//@RestController
//public class AcctBillController implements IAcctBillService {
//
//    @Autowired
//    private AcctBillService acctBillService;
//
//    @Override
//    public FeignResponse betBillCreate(@Valid @RequestBody FeignRequest feignRequest) throws RuntimeException {
//        return acctBillService.betBillCreate(feignRequest);
//    }
//
//}
