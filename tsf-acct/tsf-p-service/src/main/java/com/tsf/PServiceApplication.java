package com.tsf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.tsf.annotation.EnableTsf;

@SpringBootApplication
@EnableFeignClients
@EnableTsf
public class PServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PServiceApplication.class, args);
    }
}